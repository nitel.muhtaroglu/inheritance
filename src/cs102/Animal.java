package cs102;

public class Animal {
    private String name;
    private String color;

    public void setName(String name) {
        this.name = name;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return this.name;
    }

    public String getColor() {
        return this.color;
    }

    public String toString() {
        return "Hi, my name is " + this.name + ". I'm " + this.color + ".";
    }
}
