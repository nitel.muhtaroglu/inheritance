package cs102;

public class Cat extends Animal {
    public Cat(String name) {
        setName(name);
        setColor("gray");
    }

    public String speak() {
        return "Miyauv!";
    }

    public String toString() {
        return super.toString() + "\nI am a cat and I Miyauv.";
    }
}
